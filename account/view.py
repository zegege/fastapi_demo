from typing import List, Optional

from fastapi import APIRouter, Depends, UploadFile, File
from fastapi import Body, Query
from pydantic import BaseModel

from .views.userviews import router as user_router

router = APIRouter()


async def pagesize(q: Optional[str] = None, page: int = 0, size: int = 10):
    return {"q": q, "page": page, "size": size}


router.include_router(user_router, dependencies=[Depends(pagesize)])


class Status(BaseModel):
    message: str


class BulkIn(BaseModel):
    pk_list: List = Body(..., example=[1, 2, 3])


async def common_parameters(q: str = None, skip: int = 0, limit: int = 100):
    return {"q": q, "skip": skip, "limit": limit}


class CommonQueryParams:
    def __init__(self, q: Optional[str] = None, skip: int = 0, limit: int = 100):
        self.q = q
        self.skip = skip
        self.limit = limit

    @classmethod
    def get_obj(cls):
        return "aaa"


@router.get("/items1/")
async def read_items(commons: CommonQueryParams = Depends()):
    response = {}
    print(commons.get_obj())
    if commons.q:
        response.update({"q": commons.q})
    return response


@router.get("/items/")
async def read_items(commons: dict = Depends(common_parameters)):
    commons.update({'小钟': '同学'})
    return commons


@router.get("/items1/")
async def items(size: float = Query(1, gt=0, lt=10.5, description="大小"), q: str = None):
    return size


# 文件上传
@router.post("/upload-file/")
async def create_upload_file(file: UploadFile = File(...)):
    a = await file.read()
    await file.close()
    print(a)
    return {"filename": file.filename}


@router.post("/upload-files/")
async def create_upload_files(files: List[UploadFile] = File(...)):
    return {"filenames": [file.filename for file in files]}


@router.post("/files/", deprecated=True)
async def create_file(file: bytes = File(...)):
    return {"file_size": len(file)}
