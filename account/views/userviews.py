from typing import List, Optional

from fastapi import APIRouter, Depends
from fastapi import Body
from fastapi import HTTPException
from fastapi import Path
from fastapi import status
from pydantic import BaseModel

from account.models import User, User_Pydantic, UserIn_Pydantic

router = APIRouter()


class Status(BaseModel):
    message: str


class BulkIn(BaseModel):
    pk_list: List = Body(..., example=[1, 2, 3])


async def pagesize(q: Optional[str] = None, page: int = 0, size: int = 10):
    return {"q": q, "page": page, "size": size}


@router.post('/users', response_model=User_Pydantic, status_code=status.HTTP_201_CREATED)
async def users(user: UserIn_Pydantic = Body(
    ...,
    example={
        "username": "li-si",
        "password": "123456",
        "email": "li_si@qq.com",
        "avatar": "https://csdnimg.cn/cdn/content-toolbar/csdn-logo.png",
        "intro": "简介",
        "nickname": "李四"
    })):
    """
    添加用户接口
    """

    user_obj = await User.create(**user.dict(exclude_unset=True))
    # user_obj = await User.create(username='admin', password='123456', nickname="测试")
    return user_obj


@router.get("/users", response_model=List[Optional[User_Pydantic]])
async def users(page_size: dict = Depends(pagesize)):
    """
    用户查询
    """
    return await User.all().limit(page_size.get('size', 0)).offset(page_size.get('page', 0))


@router.get("/users/{user_id}", response_model=User_Pydantic, response_model_exclude_unset=True,
            response_model_exclude={"id"})
async def users(user_id: int) -> Optional[User]:
    """
    查询详细信息
    """
    print(user_id)
    return await User.get_or_none(id=user_id)


@router.delete("/users/{user_id}", response_model=Status, )
async def users(*, user_id: str = Path(..., title="用户id", description="用户ID")):
    """
    删除指定用户
    """
    deleted_count = await User.filter(id=user_id).delete()
    if not deleted_count:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"User {user_id} not found",
                            headers={"X-Error": "There goes my error"})
    return Status(message=f"Deleted user {user_id}")
