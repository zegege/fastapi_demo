from tortoise.contrib.pydantic import pydantic_model_creator

from config.models import User

User_Pydantic = pydantic_model_creator(User, name="User", exclude=('password',))
UserIn_Pydantic = pydantic_model_creator(User, name="UserIn", exclude_readonly=True,
                                         exclude=('is_active', 'is_staff', 'is_superuser', 'last_login'))
