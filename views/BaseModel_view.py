from datetime import date

from pydantic import BaseModel

from main import app


class Item(BaseModel):
    """
    创建数据模型
    """
    name: str
    description: str = None
    price: float
    tax: float = None
    join: date


@app.post("/items/")
async def create_item(item: Item):
    """
    :param item: 把数据模型声明为参数
    :return:
    """
    item_dict = item.dict()
    if item_dict:
        price_with = item.tax + item.price
        item_dict['price_with'] = price_with
    return item_dict


@app.put("/items/{item_id}")
async def create_item(item_id: int, item: Item):
    return {"item_id": item_id, **item.dict()}



