from enum import Enum

from main import app


class ModelName(str, Enum):
    alexnet = "亚历克斯内特"
    resnet = "雷斯网"
    lenet = "莱内特"


@app.get("/model/{model_name}")
async def get_model(model_name: ModelName):
    print(model_name.value, model_name.name)

    if model_name == ModelName.alexnet:
        return {"msg": "选中了亚历克斯内特"}
    if model_name.value == "lenet":
        return {"msg": "选中了莱内特"}
    return {"msg": "莱内特"}


fake_items_db = [{"item_name": "Foo"}, {"item_name": "Bar"}, {"item_name": "Baz"}]


@app.get("/test/")
async def read_item(page: int = 0, size: int = 10):
    """
    查询参数
    :param page: 第几页
    :param size:  每页显示几条数据
    :return:
    """
    return fake_items_db[page: page + size]


@app.get("/users/{user_id}/items/{item_id}")
async def read_user_item(user_id: int, item_id: int, q: str = None, short: bool = True):
    item = {
        "item_id": item_id,
        "user_id": user_id
    }
    if q:
        item['query'] = q
    if not short:
        item["short"] = '这个值为False'

    return item


from typing import Optional


@app.get("/int_is_none/{item_id}")
async def read_user_item(item_id: str, limit: Optional[int] = None):
    item = {"item_id": item_id, "limit": limit}
    return item
