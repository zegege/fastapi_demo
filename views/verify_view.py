from fastapi import Query

from main import app


@app.get("/items3/")
async def read_items3(
        q: str = Query(
            None, max_length=50, min_length=3,
            # 正则验证
            regex="^ces$"
        )
):
    """
    添加验证
    :param q:
    :return:
    """
    results = {"items": [{"item_id": "Foo"}, {"item_id": "Bar"}]}
    if q:
        results.update({"q": q})
    return results


from typing import List


@app.get("/items4/")
async def read_items4(q: List[str] = Query(["None", "boo"])):
    """
    查询参数列表/多个值
    :param q:
    :return:
    """
    query_items = {"q": q}
    return query_items


@app.get("/items5/")
async def read_items5(q: str = Query(
    None, title="字符串",
    description="查询字符串，用于在数据库中搜索匹配良好的项",  # 参数说明
    alias="item",  # 别名
    deprecated=True  # 废弃字段
)):
    """
    查询参数列表/多个值
    :param q:
    :return:
    """
    query_items = {"q": q}
    return query_items
