from fastapi import Path, Query

from main import app


@app.get("/path/{path_id}")
async def read_item(
        q: str,
        path_id: int = Path(..., title="要获取的项目的ID", description="要获取的项目的ID"),
):
    results = {"path_id": path_id}
    if q:
        results.update({"q": q})
    return results
