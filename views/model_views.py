import uuid
from datetime import date, datetime

from pydantic import BaseModel

from main import app


class Item(BaseModel):
    """
    指定item基础模型
    优点
        1. 类型检查
        2. 数据无效时自动清除错误。
    """
    name: str
    price: float
    is_offer: bool = None


def main(user_id: int):
    return str(user_id + 1) + str(uuid.uuid1())


class User(BaseModel):
    id: int
    name: str
    join: date
    signup_ts: datetime = None


@app.post('/user')
def create_user(user: User):
    # main(user.id)
    my_user: User = User(id=1, name=user.name, join=user.join)
    # my_user: User = User(**user)
    return my_user


@app.post("/")
@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/items/{item_id}")
def read_item(item_id: int, q: str = None):
    """

    :param item_id: 明确了类型
    :param q: q: str = None可选的字段
    :return:
    """
    return {"item_id": item_id, "q": q}
