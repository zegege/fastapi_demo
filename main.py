import account.view
import config.views
from config.settings import create_app

app = create_app()
app.include_router(account.view.router, tags=['account'])
app.include_router(config.views.router, tags=['config'])

if __name__ == '__main__':
    import uvicorn

    uvicorn.run("main:app", host='127.0.0.1', port=7000, debug=True, reload=True, lifespan="on")
