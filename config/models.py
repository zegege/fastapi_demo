from datetime import datetime, timedelta
from typing import Optional, List, Dict

import jwt
from passlib.context import CryptContext
from pydantic import EmailStr
from tortoise import fields, BaseDBAsyncClient
from tortoise.models import Model

from .basemixin import Mixin
from .enums import PermissionAction
from .settings import SECRET_KEY, ALGORITHM

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


class abcModel(Model):
    """
    基础抽象模型
    """
    id = fields.IntField(pk=True)

    class Meta:
        abstract = True


class User(abcModel, Mixin.TimestampMixin):
    """
    用户模型
    """
    # id = fields.UUIDField(pk=True)
    username = fields.CharField(max_length=20, unique=True, description="用户名")
    password = fields.CharField(max_length=200)
    email = fields.CharField(max_length=200, description="邮箱", default=None, null=True)
    last_login = fields.DatetimeField(description="Last Login", default=datetime.now)
    is_active = fields.BooleanField(default=True, description="Is Active")
    is_staff = fields.BooleanField(default=False, description="Is Staff")
    is_superuser = fields.BooleanField(default=False, description="Is SuperUser")
    avatar = fields.CharField(max_length=300, default="")
    intro = fields.TextField(default="")
    nickname = fields.CharField(max_length=99, default=None, null=True, description="昵称")

    class Meta:
        table = "user"
        table_description = "这是一个用户表"
        # unique_together=(("field_a", "field_b"), ) # 列集设置复合唯一索引
        # indexes 指定indexes为列集设置复合非唯一索引。
        #     indexes=(("field_a", "field_b"), ("field_c", "field_d", "field_e")
        ordering = ["last_login", ]

    def __str__(self):
        return self.nickname

    @classmethod
    def hash_password(cls, password: str) -> str:
        """
        将密码进行加密
        :param password:
        :return:
        """
        return pwd_context.hash(password)

    @classmethod
    def verify_password(cls, password, hashed_password) -> bool:
        """
        检测密码是否正确
        """
        return pwd_context.verify(password, hashed_password)

    def check_password(self, password) -> bool:
        """
        检查该密码是否为用户的密码
        """
        return pwd_context.verify(password, self.password)

    def create_access_token(self, data: Optional[Dict[str, str]] = None, expires_delta: timedelta = None) -> str:
        """
        根据data生成jwt
        """
        if data:
            to_encode = data.copy()
        else:
            to_encode = {}
        to_encode.update({"sub": self.username})
        if expires_delta:
            expire = datetime.utcnow() + expires_delta
        else:
            expire = datetime.utcnow() + timedelta(minutes=15)
        to_encode.update(dict(exp=expire))
        encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
        return encoded_jwt

    async def save(
            self,
            using_db: Optional[BaseDBAsyncClient] = None,
            update_fields: Optional[List[str]] = None,
            force_create: bool = False,
            force_update: bool = False,
    ) -> None:
        """
        将密码字段加密保存
        """
        self.password = self.hash_password(self.password)
        return await super().save(using_db, update_fields)

    @classmethod
    async def create_user(cls, username: str, password: str, email: Optional[EmailStr] = None) -> "User":
        return await cls(username=username, password=cls.hash_password(password), email=email)


class Permission(Model):
    """
    权限控制模型
    """
    label = fields.CharField(max_length=50)
    model = fields.CharField(max_length=50)

    action: PermissionAction = fields.IntEnumField(
        PermissionAction, default=PermissionAction.read, description="Permission Action"
    )

    def __str__(self):
        return self.label


class Role(Model):
    """
    角色
    """
    label = fields.CharField(max_length=50)
    users = fields.ManyToManyField("models.User")

    permissions: fields.ManyToManyRelation[Permission] = fields.ManyToManyField("models.Permission")

    def __str__(self):
        return self.label
