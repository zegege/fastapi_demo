from tortoise import fields


class Mixin:
    class TimestampMixin:
        """
        创建时间基础模型字段
        """

    created_at = fields.DatetimeField(null=True, auto_now_add=True, description="创建时间")
    modified_at = fields.DatetimeField(null=True, auto_now=True, description="更新时间")

    class NameMixin:
        """
        名称基础字段
        """
        name = fields.CharField(max_length=99, unique=True, description="名称")
