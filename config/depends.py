from typing import Optional

import jwt
from fastapi import Depends, HTTPException, status, Header
from fastapi.security import OAuth2PasswordBearer
from jwt import PyJWTError

from .models import User
from .settings import SECRET_KEY, ALGORITHM

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/login")


async def get_user(username) -> Optional[User]:
    """
    根据用户名查询用户
    """
    return await User.filter(username=username, is_active=True).first()


async def get_current_user(token: str = Depends(oauth2_scheme)):
    """
    根据token获得当前用户
    """
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="无法验证凭据",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        # 解码
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception

    except PyJWTError:
        raise credentials_exception
    user = await get_user(username)
    if user is None:
        raise credentials_exception
    return user


async def authenticate_user(username: str, password: str):
    """
    获取登录的用户
    """
    user = await get_user(username)
    if not user:
        return False
    if not user.check_password(password):
        return False
    return user


async def get_current_active_user(current_user: User = Depends(get_current_user)):
    """
    判断用户是否活跃
    """
    if not current_user.is_active:
        raise HTTPException(status_code=400, detail="Inactive user")
    return current_user


async def verify_token(x_token: str = Header(...)):
    if x_token != "fake-super-secret-token":
        raise HTTPException(status_code=400, detail="X-Token header invalid")


async def verify_key(x_key: str = Header(...)):
    if x_key != "fake-super-secret-key":
        raise HTTPException(status_code=400, detail="X-Key header invalid")
    return x_key