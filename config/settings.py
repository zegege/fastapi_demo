from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from tortoise.contrib.fastapi import register_tortoise

TORTOISE_ORM = {
    'connections': {
        "default": {
            "engine": "tortoise.backends.sqlite",
            "credentials": {"file_path": "data.sqlite"},
        }
    },
    'apps': {
        'models': {
            'models': ["config.models", "account.models"],
            # 'models': ["account.models", ""],
            'default_connection': 'default',
        }
    }
}


def create_app():
    fast_app = FastAPI(title="FastApi项目", debug=True, description="❄️")
    register_tortoise(
        fast_app,
        config=TORTOISE_ORM,  # 配置格式
        # db_url="sqlite://data.db",  # 使用DB_URL字符串 https://tortoise-orm.readthedocs.io/en/latest/databases.html#db-url
        # modules={"models": ['account.models']},  # models列表 定义了应为模型发现的“应用”和模块。
        generate_schemas=True,  # 如果数据库为空，则自动生成对应表单,生产环境不要开
        add_exception_handlers=True,  # 生产环境不要开，会泄露调试信息
    )
    fast_app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )
    return fast_app


SECRET_KEY = "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7"
ALGORITHM = "HS256"  # 算法
ACCESS_TOKEN_EXPIRE_MINUTES = 30  # 有效期
