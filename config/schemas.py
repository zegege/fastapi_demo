from pydantic.main import BaseModel


class Token(BaseModel):
    access_token: str
    token_type: str


class QueryItem(BaseModel):
    page: int = 1
    sort: dict
    where: dict = {}
    with_: dict = {}
    size: int = 10
    sort: dict = {}

    class Config:
        fields = {"with_": "with"}
