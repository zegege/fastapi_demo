from datetime import timedelta

from fastapi import APIRouter, HTTPException, status, Depends
from fastapi.security import OAuth2PasswordRequestForm

from account.models import User, User_Pydantic
from .depends import authenticate_user, get_current_active_user, verify_token, verify_key
from .schemas import Token
from .settings import ACCESS_TOKEN_EXPIRE_MINUTES

router = APIRouter()


@router.post("/login", response_model=Token, description="登录接口", summary="登录获取访问令牌", response_description="成功登录")
async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends()):
    user = await authenticate_user(form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="用户名或密码错误",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = user.create_access_token(
        data={"sub": user.username}, expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer"}


@router.get("/identity", response_model=User_Pydantic, dependencies=[Depends(verify_token), Depends(verify_key)])
async def read_users_me(current_user: User = Depends(get_current_active_user)):
    print("a")
    return current_user

